#include <benchmark/benchmark.hpp>

#include <random>
#include <vector>
#include <cmath>
#include <algorithm>

class SortedSet {
public:
    explicit SortedSet(const std::vector<int>& data): data_(data) {
        std::sort(data_.begin(), data_.end());
    }

    bool find(int x) const {
        auto it = std::lower_bound(data_.begin(), data_.end(), x);
        return it != data_.end() && *it == x;
    }
private:
    std::vector<int> data_;
};

class SortedSetBlocks {
public:
    explicit SortedSetBlocks(const std::vector<int>& data): data_(data) {
        std::sort(data_.begin(), data_.end());
        block_size_ = static_cast<int> (sqrt(data_.size()));
        size_t blocks_count = data_.size() / block_size_ + (data_.size() % block_size_ != 0);
        max_in_blocks_.resize(blocks_count);
        for (size_t i = block_size_, j = 0; i < data_.size(); i += block_size_, ++j)
            max_in_blocks_[j] = data_[i - 1];
        max_in_blocks_[blocks_count - 1] = data_.back();
    }

    bool find(int x) const {
        auto block_it = std::lower_bound(max_in_blocks_.begin(), max_in_blocks_.end(), x);
        if (block_it == max_in_blocks_.end())
            return false;
        size_t block_index = block_it - max_in_blocks_.begin();
        auto begin = data_.begin() + block_index * block_size_;
        auto end = block_index + 1 == max_in_blocks_.size() ? data_.end() :
            data_.begin() + (block_index + 1) * block_size_;
        auto it = std::lower_bound(begin, end, x);
        return it != end && *it == x;
    }
private:
    std::vector<int> data_;
    std::vector<int> max_in_blocks_;
    int block_size_;
};

const int kSize = 1 << 22;
const int kSeed = 8374645;

std::vector<int> gen_vector(int size, std::mt19937& gen) {
    std::vector<int> data(size);
    for (int& x : data)
        x = gen();
    return data;
}

void simple_search(benchmark::State& state) {
    std::mt19937 gen(kSeed);
    auto data = gen_vector(kSize, gen);
    SortedSet set(data);
    size_t index = 0;
    while (state.KeepRunning()) {
        bool result = set.find(data[index]);
        if (!result)
            state.SkipWithError("Bad find");
        ++index;
        if (index == data.size())
            index = 0;
    }
}

void block_search(benchmark::State& state) {
    std::mt19937 gen(kSeed);
    auto data = gen_vector(kSize, gen);
    SortedSetBlocks set(data);
    size_t index = 0;
    while (state.KeepRunning()) {
        bool result = set.find(data[index]);
        if (!result)
            state.SkipWithError("Bad find");
        ++index;
        if (index == data.size())
            index = 0;
    }
}

BENCHMARK(simple_search);
BENCHMARK(block_search);

BENCHMARK_MAIN();

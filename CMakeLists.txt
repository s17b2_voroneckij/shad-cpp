project(shad-cpp)

cmake_minimum_required(VERSION 3.8)

set(CMAKE_CXX_STANDARD             17)
set(CMAKE_MODULE_PATH              "${CMAKE_SOURCE_DIR}/cmake")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)

find_package(Catch REQUIRED)
find_package(Benchmark REQUIRED)
find_package(GTest REQUIRED)

find_package(PNG)
find_package(JPEG)
find_package(FFTW)
find_package(LLVM CONFIG)

include_directories(commons)
include(cmake/TestSolution.cmake)
include(cmake/BuildFlags.cmake)

add_subdirectory(is-prime)
add_subdirectory(reduce)
add_subdirectory(hash-table)
add_subdirectory(subset-sum)

add_subdirectory(semaphore)
add_subdirectory(rw-lock)
add_subdirectory(buffered-channel)
add_subdirectory(unbuffered-channel)

add_subdirectory(spinlock)
add_subdirectory(rw-counter)
add_subdirectory(rw-spinlock)
add_subdirectory(fast-queue)
add_subdirectory(futex)

add_subdirectory(lock-free-stack)

add_subdirectory(sfinae)
add_subdirectory(compile-eval)
add_subdirectory(constexpr-map)
add_subdirectory(concepts)

add_subdirectory(matrix-2.0)

add_subdirectory(executors)

add_subdirectory(micro-arch-quiz)
add_subdirectory(perf)

if (${PNG_FOUND} AND ${JPEG_FOUND} AND ${FFTW_FOUND})
  add_subdirectory(jpeg-decoder)
elseif()
  message(STATUS "jpeg-decoder disabled. PNG:${PNG_FOUND} JPEG:${JPEG_FOUND} PNG:${PNG_FOUND}")
endif()

if (${LLVM_FOUND})
  add_subdirectory(clang-fun)
elseif()
  message(STATUS "clang-fun disabled.")
endif()


add_benchmark(bench_jpeg bench_jpeg.cpp)

add_benchmark(bench_pi bench_pi.cpp)

add_benchmark(bench_radix_sort bench_radix_sort.cpp)

add_benchmark(bench_json bench_json.cpp)

add_benchmark(bench_logger bench_logger.cpp)

add_executable(reorder reorder.cpp)

#include <gtest/gtest.hpp>

#include <matrix.h>

TEST(Correctness, Constructors) {
    Matrix<int> a(3);
    Matrix<int> zeros{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    Matrix<int> ones{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    ASSERT_EQ(zeros, a);
    Matrix<int> b(3, InitType::ONE);
    ASSERT_EQ(ones, b);
}

TEST(Correctness, Transpose) {
    Matrix<int> a(1);
    a(0, 0) = 7;
    Matrix<int> a_ok{{7}};
    ASSERT_EQ(a_ok, transpose(a));

    Matrix<int> b({{1, 2}, {3, 4}});
    Matrix<int> b_ok{{1, 3}, {2, 4}};
    ASSERT_EQ(b_ok, transpose(b));
}

TEST(Correctness, Multiplication) {
    Matrix<int> a = Matrix<int>(3, InitType::ONE) * 4;
    Matrix<int> a_ok{{4, 0, 0}, {0, 4, 0}, {0, 0, 4}};
    Matrix<int> b{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    Matrix<int> b_ok{{2, 4, 6}, {8, 10, 12}, {14, 16, 18}};
    ASSERT_EQ(b_ok, 2 * b);

    Matrix<int> c{{0, -1, 13, 4}, {1, 3, 9, -3}, {5, 18, 2, 7}, {1, 2, 3, 4}};
    Matrix<int> d{{9, 1, 4, 13}, {83, 12, 1, 9}, {1, 1, 0, 3}, {-1, -3, -5, -7}};
    Matrix<int> mul_ok{{-74, -11, -21, 2}, {270, 55, 22, 88}, {1534, 202, 3, 184}, {174, 16, -14, 12}};
    ASSERT_EQ(mul_ok, c * d);
}

TEST(Correctness, Sum) {
    Matrix<int> a{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    Matrix<int> b{{-1, -2, -3}, {-4, -5, -6}, {-7, -8, -9}};
    ASSERT_EQ(Matrix<int>(3), a + b);
    ASSERT_EQ(a * 2, a - b);
}

TEST(Correctness, Mix) {
    Matrix<int> a{{1, 1, 1}, {0, 1, 1}, {0, 0, 1}};
    Matrix<int> b{{0, 2, 3}, {-1, 19, 17}, {3, 3, -4}};
    Matrix<int> c{{9, 1, 0}, {90, 1, 11}, {2, 6, 1}};
    Matrix<int> d{{1, 32, -3}, {4, 5, -8}, {2, 1, -2}};
    Matrix<int> ok_1{{-11, -269, 51}, {-114, -2874, 313}, {-25, -92, 52}};
    Matrix<int> ok_2{{14, 163, -12}, {109, 47, -9}, {14, 35, 7}};
    Matrix<int> ok_3{{-78 * 9, -12 * 9, -7 * 9}, {1744 * 9, -158 * 9, 398 * 9}, {1580 * 9, -211 * 9, 345 * 9}};
    Matrix<int> ok_4{{11, 36, 1}, {93, 26, 21}, {7, 10, -4}};

    ASSERT_EQ(ok_1, a * b - c * d);
    ASSERT_EQ(ok_2, transpose(a) * b + c + d * 5);
    ASSERT_EQ(ok_3, 9 * transpose(a + b) * (c - d));
    ASSERT_EQ(ok_4, a + b + c + d);

    d = a;
    c = a + b;
    ASSERT_EQ(a, d);
    ASSERT_EQ(a + b, c);
}
